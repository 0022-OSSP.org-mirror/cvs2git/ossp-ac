##
##  OSSP ac -- Auto Connection
##  Copyright (c) 2003-2004 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2004 Cable & Wireless <http://www.cw.com/>
##  Copyright (c) 2003-2004 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of OSSP ac, a tool for automated login
##  sessions which can be found at http://www.ossp.org/pkg/tool/ac/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact The OSSP Project <http://www.ossp.org>
##
##  ac.bash: GNU Bash hooks (syntax: Bash 2.x)
##

#   command line completion hook function
ac_complete () {
    #   determine current context
    local arg_pos="$COMP_CWORD"
    local arg_cur="${COMP_WORDS[COMP_CWORD]}"
    local arg_prev="${COMP_WORDS[COMP_CWORD-1]}"

    #   initialize reply
    COMPREPLY=()

    #   create cache file if still not existing
    if [ ! -f $HOME/.ac/.complete ]; then
        command ac --update
    fi

    #   complete ac command line arguments
    case "$arg_cur" in
        -* ) COMPREPLY=($(compgen -W '$(egrep -- "^option" $HOME/.ac/.complete | sed -e "s;^[^ ]* ;;")' -- "$arg_cur")) ;;
        *  ) COMPREPLY=($(compgen -W '$(egrep -- "^host"   $HOME/.ac/.complete | sed -e "s;^[^ ]* ;;")' -- "$arg_cur")) ;;
    esac
}

#   hook function into ac command line completion sequence
complete -F ac_complete ac

